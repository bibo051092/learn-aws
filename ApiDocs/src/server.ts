import express from "express";
import swaggerUi from 'swagger-ui-express';
import swaggerJsdoc from 'swagger-jsdoc';
import { s2TestSwaggerRouter } from './routers/s2TestSwagger.router';
import { pathsData, componentsData } from './handle/mergeYmlData';

const app = express();
const port = 7000;

app.use(express.json());

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'FPL API',
        version: '1.0.0',
        description: 'Everything about FPL',
    },
    servers: [
        {
            url: `http://localhost:${port}`,
            description: 'Development server',
        },
    ],
    paths: pathsData,
    components: componentsData
};

const options = {
    swaggerDefinition,
    apis: ['./server.ts'],
};

const swaggerSpec: object = swaggerJsdoc(options);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/items', s2TestSwaggerRouter);

const server = app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

/**
 * Webpack HMR Activation
 */

type ModuleId = string | number;

interface WebpackHotModule {
    hot?: {
        data: any;
        accept(
            dependencies: string[],
            callback?: (updatedDependencies: ModuleId[]) => void,
        ): void;
        accept(dependency: string, callback?: () => void): void;
        accept(errHandler?: (err: Error) => void): void;
        dispose(callback: (data: any) => void): void;
    };
}

declare const module: WebpackHotModule;

if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => server.close());
}