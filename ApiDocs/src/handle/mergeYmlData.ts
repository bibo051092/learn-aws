import * as fs from 'fs-extra';
import * as yaml from 'js-yaml';
import path from "path";

interface MergedData {
    [key: string]: any;
}

/**
 * Handle merge yml files
 * @param filePaths
 */
function mergeYmlFiles(filePaths: string[]): MergedData {
    let mergedData: MergedData = {};

    for (const filePath of filePaths) {
        try {
            const yamlContent = fs.readFileSync(filePath, 'utf8');
            const parsedYaml = yaml.load(yamlContent) as MergedData;
            mergedData = {...mergedData, ...parsedYaml};
        } catch (error) {
            console.error(`Error reading or parsing ${filePath}:`, error);
        }
    }

    return mergedData;
}

/**
 * Get File Path
 */
function getPath(pathFile: string): string {
    return path.join(__dirname, `../src/${pathFile}`);
}

/**
 * Get Swagger Paths
 */
function getSwaggerPaths(): object {
    const filePathsToMerge: string[] = [
        getPath('paths/s2TestSwagger.yml'),
    ];

    return mergeYmlFiles(filePathsToMerge);
}

/**
 * Get Swagger Components
 */
function getSwaggerComponents(): object {
    const filePathsToMerge: string[] = [
        getPath('components/schemas.yml'),
        getPath('components/parameters.yml'),
        getPath('components/common.yml'),
        getPath('components/objects.yml'),
    ];

    return mergeYmlFiles(filePathsToMerge);
}

export const pathsData: object = getSwaggerPaths();
export const componentsData: object = getSwaggerComponents();