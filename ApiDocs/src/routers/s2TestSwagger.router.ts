import express, {Request, Response} from "express";
import {lambdaHandler} from '../../../s2TestSwagger/src/app'

/**
 * Router Definition
 */
export const s2TestSwaggerRouter = express.Router();

/**
 * Controller Definitions
 */
// GET items/
s2TestSwaggerRouter.get("/", async (req: Request, res: Response) => {
    try {
        const result = await lambdaHandler(req.body);

        switch (result.statusCode) {
            case 200:
                res.status(200).json(result);
                break;
            case 400:
                res.status(400).json({
                    title: '400 BadRequest',
                    detail: 'The \'項目名\' field must be an object. [EA015]',
                    code: 400,
                    errorCode: 'EA015'
                });
                break;
            case 404:
                res.status(404).json({
                    title: '404 Not Found',
                    detail: 'The requested URL \'URI\' was not found on this server. [EA000]',
                    code: 400,
                    errorCode: 'EA000'
                });
                break;
            default:
                break;
        }
    } catch (error) {
        return res.status(500).json({
            title: '500 Internal Server Error',
            detail: 'Internal Server Error. [EA001]',
            code: 500,
            errorCode: 'EA001'
        });
    }
});

// GET items/:id

// POST items/

// PUT items/

// DELETE items/:id